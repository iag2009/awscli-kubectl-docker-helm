FROM alpine:3.16
LABEL maintainer="Bitbucket Pipelines <pipelines-feedback@atlassian.com>"

# General
RUN apk update && \
    apk upgrade && \
    apk add --no-cache curl binutils openssl git jq less bash groff mailcap libc6-compat

# Helm
RUN curl -fsSL -o get_helm.sh https://raw.githubusercontent.com/helm/helm/master/scripts/get-helm-3 \
    && chmod 700 get_helm.sh \
    && sh get_helm.sh

# S3 plugin for Helm
RUN helm plugin install https://github.com/hypnoglow/helm-s3.git

# kubectl
RUN curl -LO "https://storage.googleapis.com/kubernetes-release/release/$(curl -s https://storage.googleapis.com/kubernetes-release/release/stable.txt)/bin/linux/amd64/kubectl" \
    && chmod +x kubectl \
    && mv kubectl /usr/local/bin/kubectl 

# aws cli
ARG VERSION="1.32.84"
ENV AWS_CLI_VERSION=$VERSION
RUN apk --update --no-cache add python3 py-pip
RUN python3 -m pip install --no-cache-dir awscli==$AWS_CLI_VERSION && \
    rm -rf /var/cache/apk/* /root/.cache/pip/*

WORKDIR /root
VOLUME /root/.aws

# Keep the container running indefinitely
CMD ["tail", "-f", "/dev/null"]
